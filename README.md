# Picture Mailer

Demonstration of a full-stack Clojure microsite with CI/CD.

Designed to interface with an on-site activation kiosk. 
Visitors can take a picture, which the kiosk uploads to the server.
A QR is returned, which the visitor may scan to visit their own page on the microsite.
This page displays the picture, with options to download and send as e-mail.
Visitors enter their information in a required form, which is stored in a database.

This project is deployed at [pic.kimo.life](http://pic.kimo.life)

Picture Mailer is based on a [Luminus template](https://github.com/luminus-framework/luminus-template): 
```
lein new luminus kimok/picture-mailer +postgres +re-frame +shadow-cljs
```

[Compare the master & template branches](https://gitlab.com/kimok/picture-mailer/-/compare/template...master?from_project_id=27008100) to see what I wrote.

## Local development

A full dev environment is available via docker. To start, run 

`docker-compose -f dev-local.yml up` 

Otherwise, run `lein run dev` and `lein shadow watch app` for live clojure and clojurescript builds, and `lein test-refresh` for live testing.

Visit [localhost:3000](http://localhost:3000) to view the test page.

### Sending Emails
Picture Mailer sends emails via SendGrid. Set your API key and mailing address, as well as the site and database addresses, as environment variables:

```
	SENDGRID_API_KEY:       yourkey
	SENDGRID_EMAIL_ADDRESS: sender@domain
```

For local development, you may also set them in `dev-config.edn` at the project root:
```
{
	:sendgrid-api-key       "yourkey"
	:sendgrid-email-address "sender@domain"
}
```

## Deployment

Run `lein uberjar` to compile `target/uberjar/picture-mailer.jar` for production.

Make sure these environment variables are set before running:

```	
	DATABASE_URL:           postgresql://postgres@yourdatabase/
	POSTGRES_PASSWORD:      yourpassword
	SITE_URL:               yourwebsite
	SENDGRID_API_KEY:       yourkey
	SENDGRID_EMAIL_ADDRESS: sender@domain
```

`Dockerfile` and `.gitlab-ci.yml` can be used for automated deployments.

## Consuming the API

### Uploading images

The `/upload` endpoint expects a `POST` request with content-type `multipart/form-data`.

#### Required Fields
- **file**: an image file

#### Optional Fields
- **id**: a unique identifier for the image and associated qr code
- **redirect**: string value:
  - **"qr-image"** redirects the browser to the QR code image
  - **"frontend"** redirects the browser to the frontend

#### Example

```
POST /upload HTTP/1.1
Host: localhost
Content-Type: multipart/form-data;boundary="boundary"

--boundary
Content-Disposition: form-data; name="id"

your-qr-id
--boundary
Content-Disposition: form-data; name="file"; filename="file.png"

/path/to/your/file.png
--boundary--
```

#### Response
returns the URL of the qr-code image by default, or a redirect code (see [optional fields](#optional-fields)).

### Viewing images

Visit or `GET` the `/pic/?id=yourid&filename=yourfilename` endpoint for a frontend form displaying an image, and options to download or send it as email.

### Getting a QR code

`GET` the `/qr/:id` endpoint to download a QR code image, if it exists.

### Registering users

`POST` the `/subscribe` endpoint with the following multipart parameters:

- **email**: the unique identifier for users
- **name**: full name
- **pronoun**
- **dob** date of birth, YYYY-MM-DD
- **id** id of an associated picture

### Testing

Visit the root url `/` for testing forms which can send requests to `/upload` and `/email`. 

## Agenda

- [x] view diff against project template
- [ ] easier birthdate picker
- [ ] better validation for email
- [ ] support more email services, including a local SMTP server.
- [-] SSL cert
- [ ] cronjob to clean up old files
- [ ] more error boundaries
- [ ] more than 1 picture per database user

## License

Copyright © 2021 Kimo Knowles
