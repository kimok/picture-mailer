(ns kimok.picture-mailer.db.core-test
  (:require
   [kimok.picture-mailer.db.core :refer [*db*] :as db]
   [java-time.pre-java8]
   [luminus-migrations.core :as migrations]
   [clojure.test :refer :all]
   [next.jdbc :as jdbc]
   [kimok.picture-mailer.config :refer [env]]
   [mount.core :as mount]))
(use-fixtures
  :once
  (fn [f]
    (mount/start
     #'kimok.picture-mailer.config/env
     #'kimok.picture-mailer.db.core/*db*)
    (migrations/migrate ["migrate"] (select-keys env [:database-url]))
    (f)))
(deftest test-users
  (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
    (is (= 1 (db/create-user!
              t-conn
              {:name "Sam Smith"
               :email     "sam.smith@example.com"
               :dob       "2012-01-01"
               :pronoun   "He/him/his"
               :pictures  []}
              {})))
    (is (= {
            :name "Sam Smith"
            :email     "sam.smith@example.com"
            :dob       "2012-01-01"
            :pronoun   "He/him/his"
            :pictures  []}
           (db/get-user t-conn {:email "sam.smith@example.com"} {})))))
