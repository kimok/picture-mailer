(ns kimok.picture-mailer.handler-test
  (:require
    [clojure.test :refer :all]
    [ring.mock.request :refer :all]
    [kimok.picture-mailer.handler :refer :all]
    [kimok.picture-mailer.middleware.formats :as formats]
    [kimok.picture-mailer.config :refer [env]]
    [muuntaja.core :as m]
    [clojure.java.io :as io]
    [org.httpkit.client :as http]
    [clojure.tools.logging :as log] 
    [mount.core :as mount]))

(defn parse-json [body]
  (m/decode formats/instance "application/json" body))

(use-fixtures
  :once
  (fn [f]
    (mount/start #'kimok.picture-mailer.config/env
                 #'kimok.picture-mailer.handler/app-routes)
    (f)))

(deftest test-app
  (testing "main route"
    (let [response ((app) (request :get "/"))]
      (is (= 200 (:status response)))))
  (testing "not-found route"
    (let [response ((app) (request :get "/invalid"))]
      (is (= 404 (:status response)))))
  (testing "image upload & retrieval"
    (let [upload-response
          @(http/post "http://localhost:3000/upload"
                      {:multipart [{:name "file"
                                    :content
                                    (io/file
                                     (str (:app-root env)
                                          "/resources/public/img/warning_clojure.png"))
                                    :filename "warning_clojure.png"}
                                   
                                   {:name "id"
                                    :content (.toString (java.util.UUID/randomUUID))}]})
          qr-response
          @(http/get (:body upload-response))]
      (log/debug (:body upload-response))
      (is (= 200 (:status qr-response))))))

