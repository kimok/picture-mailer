CREATE TABLE users
(name VARCHAR(30),
 email VARCHAR(30) PRIMARY KEY,
 dob VARCHAR(30),
 pronoun VARCHAR(30),
 pictures TEXT[]);
