-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(name, email, dob, pronoun, pictures)
VALUES (:name, :email, :dob, :pronoun, :pictures)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users
SET name = :name, email = :email, dob = :dob, pronoun = pronoun, pictures = pictures
WHERE email = :email

-- :name get-user :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE email = :email

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE email = :email
