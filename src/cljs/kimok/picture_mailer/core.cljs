(ns kimok.picture-mailer.core
  (:require
    [day8.re-frame.http-fx]
    [reagent.dom :as rdom]
    [reagent.core :as r :refer [with-let]]
    [reagent.ratom :refer [atom]]
    [re-frame.core :as rf]
    [goog.events :as events]
    [goog.history.EventType :as HistoryEventType]
    [markdown.core :refer [md->html]]
    [kimok.picture-mailer.ajax :as ajax]
    [kimok.picture-mailer.events]
    [reitit.core :as reitit]
    [reitit.frontend.easy :as rfe]
    [clojure.string :as string]
    [syn-antd.form :as form]
    [syn-antd.checkbox :as checkbox]
    [syn-antd.button :as button]
    [syn-antd.input :as input]
    [syn-antd.image :as image]
    [syn-antd.select :refer [select select-option]]
    [syn-antd.date-picker :as date-picker]
    [syn-antd.spin :as spin]
    [syn-antd.space :as space]
    [moment :as moment]
    [cemerick.url :as url]
    [cljs-http.client :as http]
    [cljs.core.async :refer [<!]])
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  (:import goog.History))

(defn nav-link [uri title page]
  [:a.navbar-item
   {:href   uri
    :class (when (= page @(rf/subscribe [:common/page-id])) :is-active)}
   title])

(defn headers []
  [:link {:rel "stylesheet" :href "https://cdnjs.cloudflare.com/ajax/libs/antd/4.16.1/antd.min.css"}])

(defn as-multipart
  [params] 
  (vec (map #(apply hash-map [:name (first %)
                              :content (second %)]) (seq params))))
(defn url-param [key]
  (let [key (apply str (rest (str key)))
        params (:query (url/url (-> js/window .-location .-href)))]
    (get params key)))

(defn image-url []
  (str "/pic/" (url-param :id) "/" (url-param :filename)))

(defn basic-form
  [stage form-values]
  [form/form
   {:action           "/email"
    :enc-type         "multipart/form-data"
    :method           "POST"
    :layout           "vertical"
    :align            "center"
    :label-col        {:span 8}
    :name             "basic"
    :initial-values   {:remember true}
    :on-finish        (fn [values]
                        (go (reset! stage :loading)
                            (let [age  (.diff (moment) (.-dob ^js values) "years")
                                  values (merge
                                          (js->clj values)
                                          {"age" age
                                           "id" (url-param :id)
                                           "filename" (url-param :filename)
                                           "dob" (try (.format (.-dob ^js values) "YYYY-MM-DD")
                                                      (catch js/Object e nil))})
                                  response (<! (http/post
                                                "/subscribe"
                                                {:multipart-params
                                                 values}))]
                              (reset! form-values values)
                              (js/console.log response)
                              (js/console.log (:status response))
                              (case (:status response)
                                202 (reset! stage :submitted)
                                (do (js/console.log response)
                                    (reset! stage :error))))))
    :on-finish-failed (fn [values] (js/console.log "Failed:" values))}
   [:h1 "Do you like your picture?"]
   [:p "Enter your address to download or send."]
   [form/form-item
    {:label "Name"
     :name  "name"
     :rules [{:required true
              :message  "Please enter your name."}]}
    [input/input-raw]]
   [form/form-item
    {:label "Email address"
     :name  "email"
     :rules [{:required true
              :message  "Please enter your email address."}]}
    [input/input-raw]]
   [form/form-item
    {:label "Date of Birth"
     :name "dob"}
    [date-picker/date-picker]]
   [form/form-item
    {:label "Pronoun"
     :name "pronoun"}
    [select
     [select-option {:value "She/her/hers"} "She/her/hers"]
     [select-option {:value "They/them/theirs"} "They/them/theirs"]
     [select-option {:value "He/him/his"} "He/him/his"]]]
   [form/form-item
    [button/button {:type      "primary"
                    :html-type "submit"}
     "Submit"]]])

(defn modal-button-form [& [{:keys [label] :as attrs} & children]]
  (if-not (map? attrs)
    [modal-button-form {:label "Modal Button"} (cons attrs children)]
    (with-let [stage (atom :undone)
               form-values (atom {})]
      [:div
       (case @stage
         :closed 
         [button/button {:span 8 :on-click #(reset! stage :undone)} label]
         :undone
         [basic-form stage form-values]
         :loading
         [spin/spin]
         :submitted
         [:div
          [:h1 "Thank you!"]
          [:p "Your information has been received."]
          [:div
           [button/button {:href (image-url) :download (url-param :filename)} "Download"]
           [button/button {:on-click
                           #(go (reset! stage :loading)
                                (let [response (<! (http/post
                                                    "/email"
                                                    {:multipart-params
                                                     @form-values}))]
                                  (case (:status response)
                                    202 (reset! stage :emailed)
                                    (reset! stage :error))))}
            "Email"]]]
         :emailed
         [:div "Your email has been sent."
          [:br]
          [button/button {:href (image-url) :download (url-param :filename)} "Download"]
          [button/button
           {:on-click #(reset! stage :undone)}
           "Start over"]]
         :error
         [:div
          [:h1 "Sorry,"]
          [:p "something went wrong."]
          [button/button
           {:on-click #(reset! stage :undone)}
           "Start over"]])])))

(defn navbar [] 
  (r/with-let [expanded? (r/atom false)]
              [:nav.navbar.is-info>div.container
               [:div.navbar-brand
                [:a.navbar-item {:href "/" :style {:font-weight :bold}} "picture-mailer"]
                [:span.navbar-burger.burger
                 {:data-target :nav-menu
                  :on-click #(swap! expanded? not)
                  :class (when @expanded? :is-active)}
                 [:span][:span][:span]]]
               [:div#nav-menu.navbar-menu
                {:class (when @expanded? :is-active)}
                [:div.navbar-start
                 [nav-link "#/" "Home" :home]
                 [nav-link "#/about" "About" :about]]]]))

(defn about-page []
  [:section.section>div.container>div.content
   [:img {:src "/img/warning_clojure.png"}]])

(defn pic-page []
  [:section.section>div.container>div.content
   [space/space {:direction "vertical" :size "large" :style {:text-align "center"}}
    [image/image
     {:src (image-url)
      :width 324}]
    [:div
     [modal-button-form {:label "Email"}]]]])

(defn readme []
  [:section.section>div.container>div.content
   (when-let [docs @(rf/subscribe [:docs])]
     [:div {:dangerouslySetInnerHTML {:__html (md->html docs)}}])])

(defn page []
  (if-let [page @(rf/subscribe [:common/page])]
    [:div
     [headers]
     [page]]))

(defn navigate! [match _]
  (rf/dispatch [:common/navigate match]))

(def router
  (reitit/router
    [["/" {:name        :home
           :view        #'pic-page
           :controllers [{:start (fn [_] (rf/dispatch [:page/init-home]))}]}]
     ["/about" {:name :about
                :view #'readme}]]))

(defn start-router! []
  (rfe/start!
    router
    navigate!
    {}))

;; -------------------------
;; Initialize app
(defn ^:dev/after-load mount-components []
  (rf/clear-subscription-cache!)
  (rdom/render [#'page] (.getElementById js/document "app")))

(defn init! []
  (start-router!)
  (ajax/load-interceptors!)
  (mount-components))
