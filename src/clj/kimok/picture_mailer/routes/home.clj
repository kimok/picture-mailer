(ns kimok.picture-mailer.routes.home
  (:require
   [kimok.picture-mailer.layout :as layout]
   [kimok.picture-mailer.db.core :refer [*db*] :as db]
   [next.jdbc :as jdbc]
   [clojure.java.io :as io]
   [clojure.pprint :refer [pprint]]
   [clojure.tools.logging :as log]
   [kimok.picture-mailer.middleware :as middleware]
   [ring.util.response :refer [redirect]]
   [ring.util.http-response :as response]
   [ring.util.request :refer [body-string]]
   [com.nopolabs.clozxing.encode :as encode]
   [kimok.picture-mailer.config :refer [env]]
   [clojure.data.json :as json]
   [org.httpkit.client :as http]
   [cheshire.core :refer [parse-string generate-string]]
   [hiccup.core :as hiccup]
   [clojure.string :as str]
   [clojure.spec.alpha :as s])
  (:import
   [java.util Base64]
   [org.apache.commons.io IOUtils]))

(defn home-page [request]
  (layout/render request "home.html"))

(def resource-path "/tmp/")

(defn file-path [path & [filename]]
  (java.net.URLDecoder/decode
   (str path "/" filename)
   "utf-8"))

(defn encode-base64
  [path]
  (.encodeToString
    (java.util.Base64/getEncoder)
    (org.apache.commons.io.FileUtils/readFileToByteArray
     (io/file path))))

(defn data-uri
  [imagepath]
  (str
   "data:image/"
   (second
    (re-find #"\.(.*$)" imagepath))
   ";base64,"
   (encode-base64 imagepath)))

(defn as-multipart
  [params] 
  (vec (map #(apply hash-map [:name (first %)
                              :content (second %)]) (seq params))))
(def testd { "name" "de", "email" "fs", "dob" nil, "pronoun" nil })

(defn upload-file
  "uploads a file to the target folder"
  [path {:keys [tempfile size filename]}]
  (.mkdir (io/file path))
  (with-open [in (io/input-stream tempfile)
              out (io/output-stream (file-path path filename))]
    (io/copy in out)))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-formats]}
   ["/" {:get (fn [request]
                (layout/render request "test.html" {:docs (-> "docs/README.md" io/resource slurp)}))}]
   ["/test" {:get (fn [request]
                (layout/render request "test.html"))}]
   ["/pic/" {:get (fn [{{:keys [id filename]} :query-params :as request}]
                    (layout/render request "pic.html"))}]
   ["/upload" {:post (fn [{{id "id" file "file" redirect-to "redirect"} :multipart-params :as request}]
                       (if-let [site-url (:site-url env)]
                         (let [id (if (empty? id)
                                       (.toString (java.util.UUID/randomUUID))
                                       id)
                               qr-path (str resource-path id)
                               qr-url (str "http://" site-url "/pic/" id "/qr.jpg")
                               picture-path (str resource-path id)
                               picture-url (str "http://" site-url "/pic/" id "/" (:filename file))
                               form-url (str "http://" site-url "/pic/?id=" id "&filename=" (:filename file))
                               success (-> (response/ok form-url)
                                           (response/header "Content-Type"
                                                            "text/plain; charset=utf-8"))]
                         (try (do
                                (upload-file picture-path file)
                                (encode/to-file picture-url
                                                (io/file (str qr-path "/qr.jpg")))
                                  (case redirect-to
                                    "qr-image" (redirect qr-url)
                                    "frontend" (redirect form-url)
                                    success))
                              (catch Exception e (response/not-found))))
                         (do
                           (log/warn (str "website URL was not found, "
                                           "please set :site-url in dev-config.edn "
                                           "or $SITE-URL in your environment."))
                           (response/internal-server-error!))))}]
   ["/pic/:id/:filename" {:get (fn [{{:keys [filename id]} :path-params :as request}]
                                 (response/file-response (str resource-path id "/" filename)))}]
   ["/subscribe" {:post (fn [{{name "name" email "email" dob "dob" pronoun "pronoun"
                               id "id" filename "filename" age "age"
                               :as multipart-params} :multipart-params :as request}]
                          (let [entry {:name name
                                       :email email
                                       :dob dob
                                       :pronoun pronoun
                                       :pictures [id]}]
                            (try
                              (jdbc/with-transaction [t-conn *db*]
                                (if (db/get-user {:email email})
                                  (db/update-user! t-conn entry)
                                  (db/create-user! t-conn entry))
                                (response/accepted))
                              (catch Exception e (response/internal-server-error)))))}]
   ["/qr/:id" {:get (fn [{{:keys [id]} :path-params :as request}]
                                      (response/file-response (str resource-path id "/qr.jpg")))}]
   ["/email" {:post (fn [{{name "name" email "email" dob "dob" pronoun "pronoun"
                           id "id" filename "filename" age "age" redirect-to "redirect" :as multipart-params}
                          :multipart-params :as request}]
                      (let [image-path (str resource-path id "/" filename)
                            image (if filename (encode-base64 image-path))
                            image-uri (if filename (data-uri image-path))
                            email-response
                            @(http/post "https://api.sendgrid.com/v3/mail/send"
                                        {:headers {"Content-Type" "application/json"}
                                         :oauth-token (:sendgrid-api-key env)
                                         :body (json/write-str
                                                {:content  [{:type "text/html"
                                                             :value
                                                             (hiccup/html
                                                              [:div
                                                               [:h1 "Your very own picture."]
                                                               [:img {:alt "your picture"
                                                                      :src (str "cid:picturemailerimage")}]
                                                               [:p "Hey, " (first (str/split name #" ")) "."]
                                                               [:p "Your picture looks great."]
                                                               (if-not (= "undefined" dob)
                                                                 [:p "And so do you, for the age of " age "."])
                                                               (if-not (= "undefined" pronoun)
                                                                 [:p name ", " pronoun ". Is that correct?"])
                                                               [:p "Yours truly, " [:br]
                                                                [:a {:href "https://kimo.life"} "Kimo"]]])}]
                                                 :from
                                                 {:email (:sendgrid-email-address env)
                                                  :name "Picture Mailer"}
                                                 :subject "Your Picture"
                                                 :personalizations  [{:to
                                                                      [{:email email
                                                                        :name name}]}]
                                                 :attachments
                                                 (if image
                                                   [{:content image
                                                     :filename filename
                                                     :type (str "image/" (last (str/split filename #"\.")))
                                                     :disposition "inline"
                                                     :content_id "picturemailerimage"}])})})]
                        (if-not (= 202 (:status email-response)) (log/warn email-response))
                        (case redirect-to
                          "test-page" (redirect "/")
                          (response/status (:status email-response)))))}]])


