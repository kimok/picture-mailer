FROM theasp/clojurescript-nodejs:latest
WORKDIR /app
ADD . /app
EXPOSE 3000
RUN lein uberjar
CMD ["java", "-jar", "target/uberjar/picture-mailer.jar"]
