(ns kimok.picture-mailer.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[picture-mailer started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[picture-mailer has shut down successfully]=-"))
   :middleware identity})
