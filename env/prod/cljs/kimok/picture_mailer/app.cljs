(ns kimok.picture-mailer.app
  (:require [kimok.picture-mailer.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
