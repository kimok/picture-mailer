(ns kimok.picture-mailer.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [kimok.picture-mailer.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[picture-mailer started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[picture-mailer has shut down successfully]=-"))
   :middleware wrap-dev})
